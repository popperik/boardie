import { intCheck } from "./Common";

export default class Properties {
    bools: Map<string, boolean> = new Map();
    ints: Map<string, number> = new Map(); // TODO force use of enums as keys?
    strings: Map<string, string> = new Map();

    clone(): Properties {
        const properties = new Properties();
        properties.bools = new Map(this.bools);
        properties.ints = new Map(this.ints);
        properties.strings = new Map(this.strings);
        return properties;
    }

    newBool(name: string, value: boolean = false): void {
        if (this.bools.has(name)) {
            throw new Error(`Property [${name}] already exists`);
        }

        this.bools.set(name, value);
    }

    bool(name: string): boolean {
        this.nameCheck(this.bools, name);
        return this.bools.get(name)!;
    }

    setBool(name: string, value: boolean): void {
        this.nameCheck(this.bools, name);
        this.bools.set(name, value);
    }

    newInt(name: string, value: number = 0): void {
        intCheck(value);
        if (this.ints.has(name)) {
            throw new Error(`Property [${name}] already exists`);
        }

        this.ints.set(name, value);
    }

    int(name: string): number {
        this.nameCheck(this.ints, name);
        return this.ints.get(name)!;
    }

    changeInt(name: string, delta: number): void {
        intCheck(delta);
        this.nameCheck(this.ints, name);
        this.ints.set(name, this.ints.get(name)! + delta);
    }

    setInt(name: string, value: number): void {
        intCheck(value);
        this.nameCheck(this.ints, name);
        this.ints.set(name, value);
    }

    newString(name: string, value: string = "") {
        if (this.strings.has(name)) {
            throw new Error(`Propert [${name}] already exists`);
        }

        this.strings.set(name, value);
    }

    string(name: string): string {
        this.nameCheck(this.strings, name);
        return this.strings.get(name)!;
    }

    setString(name: string, value: string): void {
        this.nameCheck(this.strings, name);
        this.strings.set(name, value);
    }

    private nameCheck(map: Map<string, any>, name: string): void {
        if (!map.has(name)) {
            throw new Error(`Property [${name}] does not exist`);
        }
    }
}