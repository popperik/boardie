import Card from "./Card";
import { randomInt } from "./Common";
import DeckDefinition from "./DeckDefinition";
import { v4 as uuid } from "uuid";

export default class Deck {
    id = uuid();
    readonly definition: DeckDefinition;
    private cardPile: Card[];
    discardPile: Card[] = [];

    constructor(definition: DeckDefinition) {
        this.definition = definition;
        this.cardPile = this.definition.cards.map(card => new Card(card, this));
    }

    discard(card: Card): void {
        this.discardPile.push(card); // TODO check if this card is "out there" so it can actually be discarded
    }

    dummy(): Deck {
        const dummy = new Deck(this.definition);
        dummy.id = this.id;
        dummy.cardPile = this.cardPile.map(c => c.dummy());
        return dummy;
    }

    randomAndRemove(): Card {
        const index  = this.randomIndex();
        const selected = this.cardPile[index];
        this.cardPile.splice(index, 1); // Remove one element at index

        if (!selected) {
            throw new Error("Couldn't draw a card. Is the deck empty?");
        }

        return selected;
    }

    randomAndPutBack(): Card {
        const selected = this.cardPile[this.randomIndex()];

        if (!selected) {
            throw new Error("Couldn't draw a card. Is the deck empty?");
        }
        return selected;
    }

    reshuffle(): void {
        const newPiles = this.definition.reshuffleHandler(this.cardPile, this.discardPile)
        this.cardPile = newPiles[0];
        this.discardPile = newPiles[1];
    }

    private randomIndex(): number {
        if (this.cardPile.length == 0) {
            this.reshuffle(); // TODO not all games will need reshuffling.
        }
        return randomInt(this.cardPile.length);
    }
}