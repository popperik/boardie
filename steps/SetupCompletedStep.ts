import Step from "./Step";
import { AsyncHandler } from "../Common";
import Game from "../Game";

export default class extends Step<AsyncHandler<Game>> {
    constructor() {
        super("Setup completed", async (game) => await game.setupCompletedHandler(game));
    }
};