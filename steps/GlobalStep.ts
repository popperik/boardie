import Step from "./Step";
import Game from "../Game";
import { AsyncHandler } from "../Common";
import RawAction from "../actions/RawAction";

export default class GlobalStep extends Step<AsyncHandler<Game>> {

    constructor(name: string, handler?: AsyncHandler<Game>) {
        super(name, async game => this.actions.forEach(a => a.execute(game)));
        if (handler) {
            this.addAction(new RawAction(handler));
        }
    }
};