import Step from "./Step";
import { AsyncHandler, PerPlayerHandler } from "../Common";
import Game from "../Game";

export default class extends Step<AsyncHandler<Game>> {
    constructor(name: string, action: PerPlayerHandler)  {
        super(name, async (game) => {
            const promises : Promise<void>[] = [];
            for(let i = 0; i < game.data.players.length; ++i) {
                if (game.data.players[i].alive) {
                    promises.push(action(game, game.data.players[i]));
                }
            }
 
            await Promise.race(promises); // TODO kill other promises?
        });
    }
};