import Step from "./Step";
import { PlayerOrderHandler, uintCheck, DefaultPlayerOrderHandler, PerPlayerHandler, AsyncHandler } from "../Common";
import Game from "../Game";
import Player from "../Player";
import RawAction from "../actions/RawAction";

export default class extends Step<PerPlayerHandler> {
    constructor(name: string, action?: PerPlayerHandler,
         playerOrderHandler: PlayerOrderHandler = DefaultPlayerOrderHandler)  {
        super(name, async (game) => {
            // What if we don't want to start from the "starting player"?
            game.data.currentPlayer = game.data.players[game.data.startingPlayerIndex];
            game.data.currentPlayerIndex = game.data.startingPlayerIndex;
            for(;;) { // Players
                if (game.data.currentPlayer.alive) {
                    game.log(`Current player: ${game.data.currentPlayer.name}`);
                    for (let i = 0; i < this.actions.length; ++i) {
                        await this.actions[i].execute(game, game.data.currentPlayer);
                    }
                }

                const nextPlayer = this.getNextPlayer(game, playerOrderHandler);
                if (!nextPlayer) {
                    break;
                }
                game.data.currentPlayer = nextPlayer;
                game.data.currentPlayerIndex = game.data.players.indexOf(nextPlayer); // TODO do game based on player ID
            }
        });

        if (action) {
            this.addAction(new RawAction<PerPlayerHandler>(action));
        }
    }

    getNextPlayer(game: Game, playerOrderHandler: PlayerOrderHandler): Player | null {
        const player = playerOrderHandler(game, game.data.currentPlayer, game.data.currentPlayerIndex);
        if (player == null) {
            return null;
        }
        if (player instanceof Player) {
            return player;
        }

        uintCheck(player);
        return game.data.players[player] || null;
    }
};