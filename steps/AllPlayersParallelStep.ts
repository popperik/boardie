import Step from "./Step";
import { PerPlayerHandler } from "../Common";
import RawAction from "../actions/RawAction";

export default class extends Step<PerPlayerHandler> {
    constructor(name: string, action?: PerPlayerHandler)  {
        super(name, async (game) => {
            const promises : Promise<void>[] = [];
            for(let i = 0; i < game.data.players.length; ++i) {
                if (game.data.players[i].alive) {
                    for (let j = 0; j < this.actions.length; ++j) {
                        promises.push(this.actions[j].execute(game, game.data.players[i]));
                    }
                }
            }

            await Promise.all(promises);
        });

        if (action) {
            this.addAction(new RawAction(action));
        }
    }
};