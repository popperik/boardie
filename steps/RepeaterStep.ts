import Step from "./Step";
import { AsyncHandler } from "../Common";
import Game from "../Game";
import Condition from "../conditions/Condition";
import Action from "../actions/Action";

export default class RepeaterStep extends Step<AsyncHandler<Game>> {
    steps: Step[] = [];

    constructor(name: string, condition: Condition) {
        super(name, async (game) => {
            while (condition.evaluate(game)) {
                game.log(`Repeater [${name}] starting new iteration`);
                for (let i = 0; i < this.steps.length; ++i) {
                    await this.steps[i].execute(game);
                }
            }
            game.log(`Repeater [${name}] finished`);
        });
    }

    addStep(step: Step): RepeaterStep {
        this.steps.push(step);
        return this;
    }

    addAction(action: Action<AsyncHandler<Game>>): this {
        throw new Error("Repeaters may only contain steps");
    }
};