import Action from "../actions/Action";
import { AsyncHandler } from "../Common";
import Game from "../Game";

export default abstract class<T = any> {
    readonly name: string;
    readonly execute: AsyncHandler<Game>;
    readonly actions: Action<T>[] = [];

    constructor(name: string, executor: AsyncHandler<Game>) {
        this.name = name;
        this.execute = executor;
    }

    addAction(action: Action<T>) {
        this.actions.push(action);
        return this;
    }
};