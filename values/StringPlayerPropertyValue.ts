import Value from "./Value";

export default class extends Value {
    constructor(name: string, player: number) {
        super(game => game.data.players[player - 1].properties.string(name));
    }
}