import Interpolate from "../Interpolate";
import Value from "./Value";

export default class extends Value {
    constructor(value: string) {
        super(game => Interpolate(value, game));
    }
}