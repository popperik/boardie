import { Handler } from "../Common";
import Game from "../Game";

export default class Value<T = string> {

    readonly get: Handler<Game, T>

    constructor(getter: Handler<Game, T>) {
        this.get = getter;
    }
}