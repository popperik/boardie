import Game from "./Game";
import Player from "./Player";
import Decision from "./Decision";
import Choice from "./choices/Choice";

export class GameOver extends Error {}

export type Handler<T, R = void> = (t: T) => R;
export type AsyncHandler<T, R = void> = (t: T) => Promise<R>;
export type Handler2<T, U, R = void> = (t: T, u: U) => R;
export type PerPlayerHandler = Handler2<Game, Player, Promise<void>>;
export type DecisionHandler = Handler2<Decision, Player, Promise<Choice>>;
export type PlayerOrderHandler = (game: Game, currentPlayer: Player, currentPlayerIndex: number) => Player | number | null;
export type ConditionHandler = (game: Game) => boolean;

export const DefaultPlayerOrderHandler: PlayerOrderHandler = (game: Game, currentPlayer: Player, currentPlayerIndex: number) => {
    return currentPlayerIndex + 1;
}

export const NoopHandler = async (...args: any[]) => {};

export function intCheck(n: number): void {
    if (!Number.isInteger(n)) {
        throw new Error("An integer is required");
    }
}

export function uintCheck(n: number): void {
    if (n < 0 || !Number.isInteger(n)) {
        throw new Error("A non-negative integer is required");
    }
}

export function randomInt(max: number): number {
    uintCheck(max);
    return Math.floor(Math.random() * max);
}