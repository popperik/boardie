import Choice from "../choices/Choice";
import { PerPlayerHandler } from "../Common";
import Decision from "../Decision";
import Action from "./Action";

export default class extends Action<PerPlayerHandler> {

    constructor(decision: Decision) {
        super(async (game, player) => {
            await (await game.needDecision(decision, player)).apply(game, player);
        });
    }
}