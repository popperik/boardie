import { intCheck } from "../Common";
import Action from "./Action";

export default class extends Action {
    constructor(name: string, value: number = 0) {
        intCheck(value);
        super(async game => game.data.players.forEach(p => p.properties.newInt(name, value)));
    }
}