import Interpolate from "../Interpolate";
import Action from "./Action";

export default class extends Action {
    constructor(message: string) {
        super(async game => game.log(Interpolate(message, game)));
    }
}