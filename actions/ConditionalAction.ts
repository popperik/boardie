import Condition from "../conditions/Condition";
import Action from "./Action";

export default class extends Action {

    readonly condition: Condition;
    readonly actions: Action[] = [];
    readonly elseActions: Action[] = [];

    constructor(condition: Condition) {
        super(async game => {
            const actions = condition.evaluate(game) ? this.actions : this.elseActions;
            for (let i = 0; i < actions.length; ++i) {
                actions[i].execute(game);
            }
        });
        this.condition = condition;
    }

    addAction(action: Action): this {
        this.actions.push(action);
        return this;
    }

    addElseAction(action: Action): this {
        this.elseActions.push(action);
        return this;
    }
}