import Action from "./Action";

export default class extends Action {
    constructor(name: string, value?: string) {
        super(async game => game.data.properties.newString(name, value));
    }
}