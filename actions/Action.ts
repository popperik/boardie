import { AsyncHandler } from "../Common";
import Game from "../Game";

export default abstract class<T = AsyncHandler<Game>> {
    readonly execute: T;

    constructor(executor: T) {
        this.execute = executor;
    }
}