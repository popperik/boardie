import Action from "./Action";

export default class extends Action {
    constructor() {
        super(async game => { game.data.currentPlayer.alive = false });
    }
}