import Action from "./Action";
import RollHandler from "./RollHandler";

export default class RollAction extends Action {

    handlers: RollHandler[] = [];

    constructor(die?: string) {
        super(async game => {
            let roll = -1;
            if (die) {
                roll = game.die(die).roll();
            } else {
                if (game.data.dice.length == 1) {
                    roll = game.data.dice[0].roll();
                } else {
                    throw new Error("Die name is mandatory unless there's only one die");
                }
            }

            this.handlers.forEach(h => h.execute(roll, game));
        });
    }

    addHandler(handler: RollHandler): RollAction {
        this.handlers.push(handler);
        return this;
    }
}