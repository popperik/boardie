import { AsyncHandler } from "../Common";
import Game from "../Game";
import Action from "./Action";

export default class<T = AsyncHandler<Game>> extends Action<T> {
    constructor(handler: T) {
        super(handler);
    }
}