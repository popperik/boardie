import Action from "./Action";

export default class extends Action {
    constructor(propertyName: string) {
        super(async game => {
            const players = game.data.players.sort(
                (p1, p2) => p1.properties.int(propertyName) - p2.properties.int(propertyName))
            game.log("Results:");
            for (let i = 0; i < players.length; ++i) {
                const p = players[i];
                game.log(`${i + 1}. ${p.name} - ${p.properties.int(propertyName)}`);
            }
        });
    }
}