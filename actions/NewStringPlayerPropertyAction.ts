import { PerPlayerHandler } from "../Common";
import Action from "./Action";

export default class extends Action<PerPlayerHandler> {
    constructor(name: string, value?: string) {
        super(async (game, player) => player.properties.newString(name, value));
    }
}