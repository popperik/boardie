import Action from "./Action";

export default class extends Action {
    constructor(name?: string, sides?: number) {
        super(async game => {
            game.addDie(name, sides);
        })
    }
}