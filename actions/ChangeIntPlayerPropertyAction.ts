import { intCheck } from "../Common";
import Action from "./Action";

export default class extends Action {
    constructor(property: string, delta: number) {
        intCheck(delta);
        super(async game => { game.data.currentPlayer.properties.changeInt(property, delta) });
    }
}