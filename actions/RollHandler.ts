import Game from "../Game";
import Action from "./Action";

export default class RollHandler {

    readonly on: number;
    readonly actions: Action[] = [];

    constructor(on: number) {
        this.on = on;
    }

    addAction(action: Action): RollHandler {
        this.actions.push(action);
        return this;
    }

    execute(rollResult: number, game: Game): void {
        if (rollResult != this.on) {
            return;
        }

        for (let i = 0; i < this.actions.length; ++i) {
            this.actions[i].execute(game);
        }
    }
}