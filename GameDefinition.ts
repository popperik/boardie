import Game from "./Game";
import { uintCheck, AsyncHandler } from "./Common";
import DeckDefinition from "./DeckDefinition";
import Step from "./steps/Step";
import YAML from "yaml";
import ObjectParser from "./ObjectParser";

export default class GameDefinition {
    readonly name: string;
    readonly maxPlayers: number;
    readonly minPlayers: number;
    readonly decks: DeckDefinition[] = [];
    readonly steps: Step<AsyncHandler<Game>>[] = [];

    constructor(name: string, minPlayers: number = 1, maxPlayers = Number.MAX_SAFE_INTEGER) {
        uintCheck(minPlayers);
        uintCheck(maxPlayers);
        this.name = name;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }

    addDeck(name: string): DeckDefinition {
        if (this.decks.find(d => d.name == name)) {
            throw new Error("A deck with that name already exists");
        }

        const deck = new DeckDefinition(name);
        this.decks.push(deck);
        return deck;
    }

    addStep(step: Step): void {
        this.steps.push(step);
    }

    createGame(): Game {
        return new Game(this);
    }

    static fromYaml(yaml: string): GameDefinition {
        return ObjectParser.gameDefinitionFromObject(YAML.parse(yaml));
    }
}