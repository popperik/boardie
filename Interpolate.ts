import Game from "./Game";

export default function(text: string, game: Game): string {
    const result = text
        .replace(/\${player(\d*)}/g, (match, playerIndex) => { 
            const player = playerIndex ? game.data.players[playerIndex - 1] : game.data.currentPlayer
            return player.name
        })
        .replace(/\${stringProperty\(([^)]+)\)}/g, (match, propertyName) => game.data.properties.string(propertyName))
        .replace(/\${intPlayerProperty\(([^)]+)\)}/g,
            (match, propertyName) => String(game.data.currentPlayer.properties.int(propertyName)));

    if (/\${[^}]/.test(result)) {
        throw new Error(`Unknown substitution found: ${result}`);
    }

    return result;
}