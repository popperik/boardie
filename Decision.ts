import Choice from "./choices/Choice";
import { DecisionHandler } from "./Common";
import { v4 as uuid } from "uuid";

export default class Decision {
    readonly id = uuid();
    text: string;
    choices: Choice[] = [];

    constructor(text: string = "") {
        this.text = text;
    }

    aiHandler: DecisionHandler = () => { throw new Error("Missing AI handler"); };

    addChoice(choice: Choice): Choice {
        this.choices.push(choice);
        return choice;
    }
}