import { PerPlayerHandler, NoopHandler, uintCheck } from "./Common";
import { ReshuffleHandler, ReshuffleAll } from "./ReshuffleHandler";
import CardDefinition from "./CardDefinition";

export default class {

    readonly name: string;
    readonly cards: CardDefinition[] = [];

    reshuffleHandler: ReshuffleHandler = ReshuffleAll;

    constructor(name: string) {
        this.name = name;
    }

    addCard(name: string, effect: PerPlayerHandler = NoopHandler): CardDefinition {
        const card = new CardDefinition(name, effect);
        this.cards.push(card);
        return card;
    }

    addCards(name: string, effect: PerPlayerHandler = NoopHandler, count: number): CardDefinition[] {
        uintCheck(count);
        const cards = [];
        for (let i = 0; i < count; ++i) {
            cards.push(this.addCard(name, effect));
        }
        return cards;
    }
}