import { v4 as uuid } from "uuid";
import Game from "./Game";
import Player from "./Player";
import Deck from "./Deck";
import Properties from "./Properties";
import CardDefinition from "./CardDefinition";
import { Visibility } from "./Visibility";
import { NoopHandler } from "./Common";

export default class Card {
    private deck: Deck;
    readonly id = uuid();
    readonly definition: CardDefinition;
    readonly name: string;
    opponentVisibility: Visibility = Visibility.Back;
    selfVisibility: Visibility = Visibility.Full;
    properties: Properties = new Properties();

    constructor(definition: CardDefinition, deck: Deck) {
        this.definition = definition;
        this.name = this.definition.name;
        this.properties = this.definition.properties.clone();
        this.deck = deck;
    }

    async apply(game: Game, player: Player): Promise<void> {
        await this.definition.effect(game, player);
    }

    discard(): void {
        this.deck.discard(this);
    }

    dummy(): Card {
        const dummyDefinition = new CardDefinition("Card", NoopHandler);
        return new Card(dummyDefinition, this.deck); // Note we're not copying properties here (or the card ID!)
    }

    toJSON() {
        return {
            ...this,
            deck: undefined // Causes a circular dependency, so it needs to be excluded
        };
    }
}
