import Value from "../values/Value";
import Condition from "./Condition";

export default class extends Condition {
    constructor(values: Value[]) {
        if (values.length < 2) {
            throw new Error("You must supply at least two values");
        }
        super(game => values.every(v => v.get(game) == values[0].get(game)));
    }
}