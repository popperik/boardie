import Condition from "./Condition";

export default class extends Condition {
    constructor() {
        super(game => !!game.data.players.find(p => p.alive))
    }
}