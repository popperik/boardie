import { ConditionHandler } from "../Common";

export default abstract class {

    readonly evaluate: ConditionHandler; 

    constructor(evaluator: ConditionHandler) {
        this.evaluate = evaluator;
    }
};