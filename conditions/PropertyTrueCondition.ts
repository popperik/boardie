import Condition from "./Condition";

export default class extends Condition {
    constructor(propertyName: string) {
        super(game => game.data.properties.bool(propertyName));
    }
};