import GameDefinition from "../GameDefinition";
import Decision from "../Decision";
import { NoopHandler, PerPlayerHandler, randomInt } from "../Common";
import Card from "../Card";
import Game from "../Game";
import Player from "../Player";
import { ReshuffleAllButTop } from "../ReshuffleHandler";
import DeckDefinition from "../DeckDefinition";
import Choice from "../choices/Choice";
import GlobalStep from "../steps/GlobalStep";
import AllPlayersStep from "../steps/AllPlayersStep";
import SetupCompletedStep from "../steps/SetupCompletedStep";
import CardChoice from "../choices/CardChoice";
import DrawFromDeckChoice from "../choices/DrawFromDeckChoice";

const uno = new GameDefinition("UNO", 2, 10);
const MAIN_DECK = "Draw pile";
const deckDefinition = uno.addDeck(MAIN_DECK);
deckDefinition.reshuffleHandler = ReshuffleAllButTop;
const colours = ["Red", "Green", "Blue", "Yellow"];
const numbers = ["0", "1", "1", "2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "8", "8", "9"];

const SKIP = "🛇";
const REVERSE = "↺";
const DRAW_TWO = "+2";
const DRAW_FOUR = "+4";
const WILD = "Wild";

function toDraw(game: Game): number {
    return game.data.properties.int("toDraw");
}

function changeToDraw(game: Game, delta: number): void {
    game.data.properties.changeInt("toDraw", delta);
}

function resetToDraw(game: Game): void {
    game.data.properties.setInt("toDraw", 0);
}

function actionNeeded(game: Game): boolean {
    return game.data.properties.bool("actionNeeded");
}

function setActionNeeded(game: Game, value: boolean) {
    game.data.properties.setBool("actionNeeded", value);
}

function addCard(deck: DeckDefinition, colour: string, value: string, count: number = 1, effect: PerPlayerHandler = NoopHandler): void {
    deck.addCards(`${colour} ${value}`, effect, count).forEach(card => {
        card.properties.newString("colour", colour);
        card.properties.newString("value", value);
    });
}

const playerOrderHandler = (game: Game, currentPlayer: Player, currentPlayerIndex: number) => {
    if (!game.data.properties.bool("inProgress")) {
        return null;
    }

    if (game.data.properties.bool("reversed")) {
        return currentPlayerIndex == 0 ? game.data.players.length - 1 : currentPlayerIndex - 1;
    } else {
        return currentPlayerIndex == game.data.players.length - 1 ? 0 : currentPlayerIndex + 1;
    }
};

colours.forEach(colour => {
    numbers.forEach(number => addCard(deckDefinition, colour, number));
    addCard(deckDefinition, colour, SKIP, 2, async (game, player) => setActionNeeded(game, true));
    addCard(deckDefinition, colour, REVERSE, 2, async (game, player) => {
        if (game.data.players.length == 2) { // For 2-player games, REVERSE == SKIP
            setActionNeeded(game, true);
            return;
        }
        
        if (!game.data.properties.bool("reversed")) {
            game.data.properties.setBool("reversed", true);
            game.data.startingPlayerIndex = game.data.players.length - 1;
        } else {
            game.data.properties.setBool("reversed", false);
            game.data.startingPlayerIndex = 0;
        }
    });
    addCard(deckDefinition, colour, DRAW_TWO, 2, async (game, player) => {
        setActionNeeded(game, true);
        changeToDraw(game, 2);
    });
});

async function colourDecision(game: Game, player: Player) {
    const colourDecision = new Decision("What colour do you want?");
    const choices = new Map<string, Choice>();
    colours.forEach(colour =>
        choices.set(colour, colourDecision.addChoice(new Choice(colour,
             async (game, player) => game.data.properties.setString("wildColour", colour)))));
    colourDecision.aiHandler = async (decision, player) => {
        // For now, we'll just go with the colour of the first card
        // TODO Use whatever we have the most of
        const choice = choices.get(player.hand[0].properties.string("colour"));
        if (choice) {
            return choice;
        }

        // If the first card is WILD, we'll just go with a random colour!
        return choices.get(colours[randomInt(3)])!;
    };
    await (await game.needDecision(colourDecision, player)).apply(game, player);
}

deckDefinition.addCards(DRAW_FOUR, async (game, player) => {
    setActionNeeded(game, true);
    changeToDraw(game, 4);
    await colourDecision(game, player);
}, 4).forEach(card => {
    card.properties.newString("colour", WILD);
    card.properties.newString("value", DRAW_FOUR);
});

deckDefinition.addCards(WILD, async (game, player) => await colourDecision(game, player), 4).forEach(card => {
    card.properties.newString("colour", WILD);
    card.properties.newString("value", WILD);
});

function isValidMove(top: Card, card: Card, game: Game): boolean {
    const cardValue = card.properties.string("value");
    const cardColour = card.properties.string("colour");
    const topValue = top.properties.string("value");
    let topColour = top.properties.string("colour");
    if (topColour == "Wild") {
        topColour = game.data.properties.string("wildColour");
    }

    if (topValue == DRAW_FOUR) {
        if (actionNeeded(game)) {
            return cardValue == DRAW_FOUR;
        } else {
            return cardColour == topColour;
        }
    } else if (topValue == DRAW_TWO) {
        if (actionNeeded(game)) {
            return cardValue == DRAW_TWO || cardValue == DRAW_FOUR;
        }
    }

    return topColour == cardColour ||
        topValue == cardValue ||
        cardColour == WILD;
}

uno.addStep(new GlobalStep("Setup", async (game) => {
    game.data.properties.newBool("inProgress", true);
    game.data.properties.newBool("reversed");
    game.data.properties.newInt("toDraw");
    game.data.properties.newBool("actionNeeded");
    game.data.properties.newString("wildColour");
    const deck = game.deck(MAIN_DECK)
    do {
        deck.discard(deck.randomAndRemove());
    } while (deck.discardPile[deck.discardPile.length - 1].properties.string("colour") == WILD); // TODO how about +2, skip and reverse?
}));

uno.addStep(new AllPlayersStep("Player setup", async (game: Game, player: Player) => 
    { player.drawAndKeepMany(game.deck(MAIN_DECK), 7)}));
uno.addStep(new SetupCompletedStep());

function getPlayCardHandler(card: Card): PerPlayerHandler {
    return async (game, player) => {
        game.log(`${player.name} plays ${card.name}`);
        await card.apply(game, player);
        player.discard(card);
    };
}

async function handleCardPlay(game: Game, player: Player): Promise<void> {
    const decision = new Decision("What's your move?");
    decision.aiHandler = async (decision, player) => {
        if (decision.choices.length == 1) { // We have to draw a card
            return decision.choices[0];
        }
        return decision.choices[randomInt(decision.choices.length - 2) + 1]; // The AI won't draw a card if it can avoid it
    };
    const deck = game.deck(MAIN_DECK);

    const top = deck.discardPile[deck.discardPile.length - 1];
    const topValue = top.properties.string("value");
    const playableCards = player.hand.filter(card => isValidMove(top, card, game));

    // Special turn, need to handle +2/+4
    if (actionNeeded(game) && (topValue == DRAW_TWO || topValue == DRAW_FOUR)) {
        setActionNeeded(game, false);
        decision.addChoice(new Choice(`Draw ${toDraw(game)} cards`, async (game, player) => {
            player.drawAndKeepMany(deck, toDraw(game));
            resetToDraw(game);
        }));
    } else { // Normal turn, nothing special to do
        decision.addChoice(new DrawFromDeckChoice(deck, async (game, player) => {
            const newCard = player.drawAndKeep(deck);
            if (isValidMove(top, newCard, game)) {
                const newCardDecision = new Decision(`You can play the ${newCard.name} you just picked up. What do you do?`);
                const playIt = newCardDecision.addChoice(new CardChoice(newCard, getPlayCardHandler(newCard)));
                newCardDecision.addChoice(new Choice("Do nothing", NoopHandler));
                newCardDecision.aiHandler = async () => playIt;
                await (await game.needDecision(newCardDecision, player)).apply(game, player);
            }
        }));
    }

    playableCards.forEach(card => decision.addChoice(new CardChoice(card, getPlayCardHandler(card))));
    await (await game.needDecision(decision, player)).apply(game, player);
}

// The player order handler will loop around, so we don't need a repeater
uno.addStep(new AllPlayersStep("Player turn", async (game, player) => {
    const deck = game.deck(MAIN_DECK);
    const top = deck.discardPile[deck.discardPile.length - 1];
    const topValue = top.properties.string("value");
    if (!player.hand.length &&
         (!actionNeeded(game) || actionNeeded(game) && topValue != DRAW_TWO && topValue != DRAW_FOUR)) {
            game.log(`${player.name} WINS!!!`);

        // Not the nicest, but this will cause the player order handler to return null, breaking out of the loop
        game.data.properties.setBool("inProgress", false);
        return;
    }

    game.log(`The top card is ${top.name}`);

    if (actionNeeded(game) && (topValue == SKIP || game.data.players.length == 2 && topValue == REVERSE)) {
        setActionNeeded(game, false);
        game.log(`${player.name}'s turn is skipped!`);
        return;
    }

    await handleCardPlay(game, player);
}, playerOrderHandler));

export default uno;