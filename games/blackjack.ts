import GameDefinition from "../GameDefinition";
import Decision from "../Decision";
import Player from "../Player";
import Game from "../Game";
import GlobalStep from "../steps/GlobalStep";
import AllPlayersStep from "../steps/AllPlayersStep";
import SetupCompletedStep from "../steps/SetupCompletedStep";
import RepeaterStep from "../steps/RepeaterStep";
import Choice from "../choices/Choice";
import PropertyTrueCondition from "../conditions/PropertyTrueCondition";

const blackjack = new GameDefinition("Blackjack");
const MAIN_DECK = "Draw pile";
const deckDefinition = blackjack.addDeck(MAIN_DECK);
const suits = ["Hearts", "Clubs", "Diamonds", "Spades"];

const getSum = (player: Player): number => player.properties.int("sum");
const changeSum = (player: Player, delta: number): void => player.properties.changeInt("sum", delta);

suits.forEach(suit => {
    deckDefinition.addCard(`Two of ${suit}`,   async (game, player) => changeSum(player, 2));
    deckDefinition.addCard(`Three of ${suit}`, async (game, player) => changeSum(player, 3));
    deckDefinition.addCard(`Four of ${suit}`,  async (game, player) => changeSum(player, 4));
    deckDefinition.addCard(`Five of ${suit}`,  async (game, player) => changeSum(player, 5));
    deckDefinition.addCard(`Six of ${suit}`,   async (game, player) => changeSum(player, 6));
    deckDefinition.addCard(`Seven of ${suit}`, async (game, player) => changeSum(player, 7));
    deckDefinition.addCard(`Eight of ${suit}`, async (game, player) => changeSum(player, 8));
    deckDefinition.addCard(`Nine of ${suit}`,  async (game, player) => changeSum(player, 9));
    deckDefinition.addCard(`Ten of ${suit}`,   async (game, player) => changeSum(player, 10));
    deckDefinition.addCard(`Jack of ${suit}`,  async (game, player) => changeSum(player, 10));
    deckDefinition.addCard(`Queen of ${suit}`, async (game, player) => changeSum(player, 10));
    deckDefinition.addCard(`King of ${suit}`,  async (game, player) => changeSum(player, 10));
    deckDefinition.addCard(`Ace of ${suit}`,   async (game, player) => {
        if (getSum(player) <= 10) { // TODO technically this is the player's choice?
            changeSum(player, 11);
        } else {
            changeSum(player, 1);
        }
    });
});

function hit(game: Game, player: Player) {
    const deck = game.deck(MAIN_DECK);
    let card = player.drawAndKeep(deck);
    game.log(`${player.name} drew the ${card.name}`);
    card.apply(game, player);
    game.log(`${player.name} now has ${getSum(player)}`);
    if (getSum(player) > 21) {
        game.log(`${player.name} is out!`);
        player.alive = false;
    } else if (getSum(player) == 21) {
        game.log(`${player.name} got a blackjack!!!`);
        player.alive = false;
    };
}

const hitOrStay = new Decision("Hit or Stay?");
const hitChoice = hitOrStay.addChoice(new Choice("Hit", async (game, player) => {
    hit(game, player);
}));
const stayChoice = hitOrStay.addChoice(new Choice("Stay", async (game, player) => {
    game.log(`${player.name} is staying.`)
    player.alive = false;
}));

hitOrStay.aiHandler = async (decision, target) => {
    if (getSum(target) < 17) {
        return hitChoice;
    }
    return stayChoice;
};

blackjack.addStep(new GlobalStep("Setup", async (game) => {
    game.addPlayer("Dealer", true);
    game.data.properties.newBool("inProgress", true);
}));
blackjack.addStep(new AllPlayersStep("Player setup", async (game, player) => {
    player.properties.newInt("sum");
    const deck = game.deck(MAIN_DECK);
    const card1 = player.drawAndKeep(deck);
    const card2 = player.drawAndKeep(deck);
    card1.apply(game, player);
    card2.apply(game, player);
    game.log(`${player.name} starts with the ${card1.name} and ${card2.name}. They now have ${getSum(player)}`);
}));
blackjack.addStep(new SetupCompletedStep());
blackjack.addStep(
    new RepeaterStep("Player turns", new PropertyTrueCondition("inProgress"))
        .addStep(new AllPlayersStep("Player turn", async (game, player) => {
            if (player.ai) {
                if (game.data.players.filter(player => !player.ai).every(player => getSum(player) >= 21)) {
                    game.data.properties.setBool("inProgress", false);
                    return; // Dealer doesn't draw if everyone busted or stayed
                }
            }
            await (await game.needDecision(hitOrStay, player)).apply(game, player);
        })));

blackjack.addStep(new GlobalStep("Results", async game => {
    const dealer: Player = game.data.players.pop() as Player; // We cannot have less than two players (including dealer)
    const dealerSum = getSum(dealer);
    game.data.players.forEach(player => {
        const playerSum = getSum(player);
        if (playerSum > 21) {
            game.log(`${player.name} LOSES by getting ${playerSum} :(`);
            return;
        } else if (dealerSum > 21) {
            game.log(`${dealer.name} busted and ${player.name} WINS with ${playerSum}!`);
            return;
        }
        
        const diff = getSum(player) - getSum(dealer);
        if (diff > 0) {
            game.log(`${player.name} WINS with ${playerSum} vs ${dealer.name}'s ${dealerSum}`);
        } else if (diff < 0) {
            game.log(`${player.name} LOSES with ${playerSum} vs ${dealer.name}'s ${dealerSum}`);
        } else {
            game.log(`${player.name} and ${dealer.name} TIE by getting ${playerSum}`);
        }
    });
}));

export default blackjack;