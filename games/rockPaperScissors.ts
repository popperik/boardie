import GameDefinition from "../GameDefinition";
import Decision from "../Decision";
import {randomInt} from "../Common";
import SetupCompletedStep from "../steps/SetupCompletedStep";
import AllPlayersParallelStep from "../steps/AllPlayersParallelStep";
import GlobalStep from "../steps/GlobalStep";
import Choice from "../choices/Choice";

const ROCK = "Rock";
const PAPER = "Paper";
const SCISSORS = "Scissors";
const decision = new Decision("Rock, paper or scissors?");

[ROCK, PAPER, SCISSORS].map(v =>
    decision.addChoice(new Choice(v, async (game, player) => player.properties.newString("choice", v)))
);

decision.aiHandler = async (game, player) => decision.choices[randomInt(2)];

const rockPaperScissors = new GameDefinition("Rock, paper, scissors", 2, 2);
rockPaperScissors.addStep(new SetupCompletedStep());
rockPaperScissors.addStep(new AllPlayersParallelStep("decision", async (game, player) => {
    (await game.needDecision(decision, player)).apply(game, player);
    player.alive = false;
}));
rockPaperScissors.addStep(new GlobalStep("results", async (game) => {
    const p1 = game.data.players[0];
    const p2 = game.data.players[1];
    const choice1 = p1.properties.string("choice");
    const choice2 = p2.properties.string("choice");

    if (choice1 == ROCK && choice2 == PAPER) {
        game.log(`${p1.name} won the game`);
    } else if (choice1 == ROCK && choice2 == SCISSORS) {
        game.log(`${p2.name} won the game`);
    } else if (choice1 == PAPER && choice2 == ROCK) {
        game.log(`${p2.name} won the game`);
    } else if (choice1 == PAPER && choice2 == SCISSORS) {
        game.log(`${p2.name} won the game`);
    } else if (choice1 == SCISSORS && choice2 == PAPER) {
        game.log(`${p1.name} won the game`);
    } else if (choice1 == SCISSORS && choice2 == ROCK) {
        game.log(`${p2.name} won the game`);
    } else if (choice1 === choice2) {
        game.log("You are tied");
    }
}));

export default rockPaperScissors;
