import GameDefinition from "../GameDefinition";
import SetupCompletedStep from "../steps/SetupCompletedStep";
import AllPlayersStep from "../steps/AllPlayersStep";
import GlobalStep from "../steps/GlobalStep";
import RepeaterStep from "../steps/RepeaterStep";
import AnyPlayerStep from "../steps/AnyPlayerStep";
import Decision from "../Decision";
import CardChoice from "../choices/CardChoice";
import PropertyTrueCondition from "../conditions/PropertyTrueCondition";

const theMind = new GameDefinition("The Mind", 2, 4);

const deck = theMind.addDeck("Numbers");

for (let i = 1; i <= 99; ++i) {
    deck.addCard(String(i), async (game, player) => {
        game.log(`${player.name} plays ${i}`);

        const remainingNumbers = game.data.players.flatMap(p => p.hand) // Get all hands
           .map(c => Number(c.name)); // Extract the value from the name :|

        if (remainingNumbers.length == 0) {
            game.log("No more cards left, you win the round!");
            game.data.properties.setBool("roundInProgress", false);
        }

        // Does anyone have a card lower than what's being played?
        const lowest = Math.min(...remainingNumbers);
        if (i > lowest) {
            game.log("You lose"); // TODO only lose a life, cancel other players' decisions
            game.data.properties.setBool("roundInProgress", false);
            game.data.properties.setBool("inProgress", false);
        } else {
            game.data.properties.setInt("lastCard", i);
        }
    });
}

theMind.addStep(new GlobalStep("Game setup", async (game) => {
    game.data.properties.newInt("lastCard");
    game.data.properties.newBool("inProgress", true);
    game.data.properties.newBool("roundInProgress");
    game.data.properties.newInt("round", 0);
}))
theMind.addStep(new SetupCompletedStep());

theMind.addStep(new RepeaterStep("Game", new PropertyTrueCondition("inProgress"))
    .addStep(new GlobalStep("Round setup", async (game) => {
        game.data.properties.setInt("lastCard", 0);
        game.data.properties.setBool("roundInProgress", true);
        game.data.properties.changeInt("round", 1);
    }))
    .addStep(new AllPlayersStep("Draw cards", async (game, player) => {
        player.drawAndKeepMany(game.deck("Numbers"), game.data.properties.int("round"));
    }))
    .addStep(new RepeaterStep("Round", new PropertyTrueCondition("roundInProgress"))
        .addStep(new AnyPlayerStep("Play a card", async (game, player) => {
            const decision = new Decision("What's your move?");
            player.hand.forEach(c => decision.addChoice(new CardChoice(c, async (game, player) => {
                player.discard(c);
                await c.apply(game, player);
            })));
            // TODO raise hand, shuriken, etc.
            (await game.needDecision(decision, player)).apply(game, player);
    }))));

// TODO game over?

export default theMind;