import Choice from "./Choice";
import Card from "../Card";
import { PerPlayerHandler } from "../Common";
import { ChoiceType } from "./ChoiceType";

export default class extends Choice {
    card: Card;

    constructor(card: Card, effect: PerPlayerHandler) {
        super(card.name, effect, ChoiceType.Card);
        this.card = card;
    }
}