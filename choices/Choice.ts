import { PerPlayerHandler } from "../Common";
import Game from "../Game";
import Player from "../Player";
import { v4 as uuid } from "uuid";
import { ChoiceType } from "./ChoiceType";
import Action from "../actions/Action";
import RawAction from "../actions/RawAction";

export default class Choice {
    readonly id = uuid();
    readonly type: ChoiceType;
    readonly actions: Action<PerPlayerHandler>[] = [];
    text: string;

    constructor(text: string, effect?: PerPlayerHandler, type: ChoiceType = ChoiceType.Generic) {
        this.text = text;
        this.type = type;

        if (effect) {
            this.addAction(new RawAction(effect));
        }
    }

    addAction(action: Action<PerPlayerHandler>): this {
        this.actions.push(action);
        return this;
    }

    async apply(game: Game, player: Player): Promise<void> {
        for (let i = 0; i < this.actions.length; ++i) {
            await this.actions[i].execute(game, player);
        }
    }
}