import Choice from "./Choice";
import Deck from "../Deck";
import { ChoiceType } from "./ChoiceType";
import { PerPlayerHandler } from "../Common";

export default class extends Choice {

    readonly deck: Deck;

    constructor(deck: Deck, effect?: PerPlayerHandler) {
        const defaultEffect: PerPlayerHandler =  async (game, player) => { player.drawAndKeep(deck) };
        super(`Draw from ${deck.definition.name}`, effect ?? defaultEffect, ChoiceType.DrawFromDeckChoice);

        this.deck = deck;
    }
}