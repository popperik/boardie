import Card from "./Card";

export type ReshuffleHandler = (cardPile: Card[], discardPile: Card[]) => [Card[], Card[]]; // returns [newCardPile, newDiscardPile]

export const ReshuffleAll: ReshuffleHandler = (cardPile: Card[], discardPile: Card[]) =>
    [discardPile.slice(), []]; // TODO add to cardPile if there are cards left? Now we're overwriting any remaining cards

export const ReshuffleAllButTop: ReshuffleHandler = (cardPile: Card[], discardPile: Card[]) =>
    // TODO add to cardPile if there are cards left? Now we're overwriting any remaining cards
    [discardPile.slice(0, discardPile.length - 1), [discardPile[discardPile.length - 1]]];