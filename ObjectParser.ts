import Action from "./actions/Action";
import AddDieAction from "./actions/AddDieAction";
import ChangeIntPlayerPropertyAction from "./actions/ChangeIntPlayerPropertyAction";
import ConditionalAction from "./actions/ConditionalAction";
import DecisionAction from "./actions/DecisionAction";
import KillPlayerAction from "./actions/KillPlayerAction";
import MessageAction from "./actions/MessageAction";
import NewIntPlayerPropertyAction from "./actions/NewIntPlayerPropertyAction";
import NewStringPlayerPropertyAction from "./actions/NewStringPlayerPropertyAction";
import NewStringPropertyAction from "./actions/NewStringPropertyAction";
import PrintLeaderboardAction from "./actions/PrintLeaderboardAction";
import RollAction from "./actions/RollAction";
import RollHandler from "./actions/RollHandler";
import Choice from "./choices/Choice";
import { AsyncHandler } from "./Common";
import AnyAliveCondition from "./conditions/AnyAliveCondition";
import Condition from "./conditions/Condition";
import EqualCondition from "./conditions/EqualCondition";
import Decision from "./Decision";
import Game from "./Game";
import GameDefinition from "./GameDefinition";
import AllPlayersParallelStep from "./steps/AllPlayersParallelStep";
import AllPlayersStep from "./steps/AllPlayersStep";
import GlobalStep from "./steps/GlobalStep";
import RepeaterStep from "./steps/RepeaterStep";
import SetupCompletedStep from "./steps/SetupCompletedStep";
import Step from "./steps/Step";
import StringPlayerPropertyValue from "./values/StringPlayerPropertyValue";
import StringValue from "./values/StringValue";
import Value from "./values/Value";

export default class {
    static gameDefinitionFromObject(game: any): GameDefinition {
        const gameDefinition = new GameDefinition(game.name, game.minPlayers, game.maxPlayers);
        game.steps?.forEach((s: any) => gameDefinition.addStep(this.stepFromObject(s)));
        return gameDefinition;
    }

    private static stepFromObject(step: any): Step<AsyncHandler<Game>> {
        // TODO error handling for missing attributes
        switch (step.type) {
            case "AllPlayersStep":
                return this.addActionsToStepFromObject(new AllPlayersStep(step.name), step.actions);
            case "AllPlayersParallelStep":
                return this.addActionsToStepFromObject(new AllPlayersParallelStep(step.name), step.actions);
            case "GlobalStep":
                return this.addActionsToStepFromObject(new GlobalStep(step.name), step.actions);
            case "RepeaterStep":
                return this.addStepsToRepeaterFromObject(
                    new RepeaterStep(step.name, this.conditionFromObject(step.condition)), step.steps);
            case "SetupCompletedStep":
                return new SetupCompletedStep();
            default:
                throw new Error(`Could not parse step: ${JSON.stringify(step)}`);
        }
    }

    private static actionFromObject(action: any): Action<any> {
        switch (action.type) {
            case "AddDieAction":
                return new AddDieAction(action.name, action.sides);
            case "ChangeIntPlayerPropertyAction":
                return new ChangeIntPlayerPropertyAction(action.name, action.delta);
            case "ConditionalAction":
                return this.addActionsToConditionalActionFromObject(
                    new ConditionalAction(this.conditionFromObject(action.condition)),
                    action.actions, action.elseActions);
            case "DecisionAction":
                return new DecisionAction(this.decisionFromObject(action));
            case "KillPlayerAction":
                return new KillPlayerAction();
            case "MessageAction":
                return new MessageAction(action.message);
            case "NewIntPlayerPropertyAction":
                return new NewIntPlayerPropertyAction(action.name, action.value);
            case "NewStringPropertyAction":
                return new NewStringPropertyAction(action.name, action.value);
            case "NewStringPlayerPropertyAction":
                return new NewStringPlayerPropertyAction(action.name, action.value);
            case "PrintLeaderboardAction":
                return new PrintLeaderboardAction(action.property);
            case "RollAction":
                return this.addHandlersToRollFromObject(new RollAction(action.die), action.handlers);
            default:
                throw new Error(`Could not parse action: ${JSON.stringify(action)}`);
        }
    }

    private static conditionFromObject(condition: any): Condition {
        switch (condition.type) {
            case "AnyAliveCondition":
                return new AnyAliveCondition();
            case "EqualCondition":
                return new EqualCondition(this.valuesFromObject(condition.values));
            default:
                throw new Error(`Could not parse condition: ${JSON.stringify(condition)}`);
        }
    }

    private static valueFromObject(value: any): Value {
        if (typeof(value) == 'string') {
            return new StringValue(value);
        }

        switch (value.type) {
            case "StringPlayerPropertyValue":
                return new StringPlayerPropertyValue(value.name, Number(value.player));
            default:
                throw new Error(`Failed to parse value: ${JSON.stringify(value)}`);
        }
    }

    private static decisionFromObject(decision: any): Decision {
        const d = new Decision(decision.text);
        decision.choices?.forEach((c: any) =>
            this.addActionsToChoiceFromObject(d.addChoice(new Choice(c.text)), c.actions));
        return d;
    }

    private static addActionsToStepFromObject(step: Step, actions: any[]): Step {
        actions?.forEach((a: any) => step.addAction(this.actionFromObject(a)));
        return step;
    }

    private static addStepsToRepeaterFromObject(step: RepeaterStep, steps: any[]): RepeaterStep {
        steps?.forEach((s: any) => step.addStep(this.stepFromObject(s)));
        return step;
    }

    private static addHandlersToRollFromObject(roll: RollAction, handlers: any[]): RollAction {
        handlers?.forEach((h: any) => roll.addHandler(this.addActionsToRollHandlerFromObject(
            new RollHandler(h.on), h.actions)));
        return roll;
    }

    private static addActionsToRollHandlerFromObject(handler: RollHandler, actions: any[]): RollHandler {
        actions?.forEach((a: any) => handler.addAction(this.actionFromObject(a)));
        return handler;
    }

    private static addActionsToConditionalActionFromObject(action: ConditionalAction,
         actions: any[], elseActions: any[]): ConditionalAction {
        actions?.forEach((a: any) => action.addAction(this.actionFromObject(a)));
        elseActions?.forEach((a: any) => action.addElseAction(this.actionFromObject(a)));
        return action;
    }

    private static addActionsToChoiceFromObject(choice: Choice, actions: any[]): Choice {
        actions?.forEach((a: any) => choice.addAction(this.actionFromObject(a)));
        return choice;
    }

    private static valuesFromObject(object: any[]): Value[] {
        return object?.map(v => this.valueFromObject(v));
    }
}