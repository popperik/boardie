import games from "./boardie";
import Decision from "./Decision";
import Player from "./Player";
import Choice from "./choices/Choice";
import rl from "readline-sync";

async function main() {
    console.log("Welcome to Boardie's test utility.")

    const index = rl.keyInSelect(games.gameDefinitions.map(g => g.name), "Select the game you would like to play: ",
        {cancel: false});

    const game = games.gameDefinitions[index].createGame();
    game.logHandler = console.log;
    game.decisionHandler = async (decision: Decision, target: Player): Promise<Choice>  => {
        const choice = rl.keyInSelect(decision.choices.map(c => c.text), decision.text, {cancel: false});
        return decision.choices[choice];
    }

    game.addPlayer();
    game.addPlayer();

    console.log("Starting simulation")
    await game.play();
}

main()
    .then(() => console.log("Simulation done"))
    .catch(e => console.log("Simulation failed! Error: ", e));