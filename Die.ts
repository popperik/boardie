import { uintCheck, randomInt } from "./Common";

export default class Die {
    readonly name: string;
    readonly sides: number;

    constructor(name: string = "Die", sides: number = 6) {
        uintCheck(sides);
        this.name = name;
        this.sides = sides;
    }

    roll(): number {
        return randomInt(this.sides) + 1;
    }
}