import blackjack from "./games/blackjack";
import uno from "./games/uno";
import rockPaperScissors from "./games/rockPaperScissors";
import theMind from "./games/theMind";
import GameDefinition from "./GameDefinition";
import fs from "fs";

export default { // TODO give this types
    gameDefinitions: [blackjack, uno, rockPaperScissors, theMind,
    GameDefinition.fromYaml(fs.readFileSync("../boardie/games/coinToss.yaml").toString("utf-8")),
    GameDefinition.fromYaml(fs.readFileSync("../boardie/games/prisonersDilemma.yaml").toString("utf-8"))]
};
